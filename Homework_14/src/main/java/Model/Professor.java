package Model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table (name = "professors")
public class Professor {

    @Id
    @GeneratedValue ( strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String lastname;

    @ManyToMany
    @JoinTable(
            name = "professors_students",
            joinColumns = @JoinColumn(name = "professor_id"),
            inverseJoinColumns = @JoinColumn (name = "student_id")
    )
    private List<Student> students;

    public Professor(){

    }

    public Professor(String name, String lastname) {
        this.name = name;
        this.lastname = lastname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public List<Student> getStudents() {
        return students;
    }

    @Override
    public String toString() {
        return "Professor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastname= '" + lastname + '\'' +
                '}';
    }
}
