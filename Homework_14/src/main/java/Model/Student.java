package Model;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table (name = "students")
public class Student {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String lastname;
    private Integer score;



    @ManyToMany
    @JoinTable(
            name = "professors_students",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn (name = "professor_id")
    )
    private List<Professor> professors;



    public Student() {

    }

    public Student(String name, String lastname, Integer score) {
        this.name = name;
        this.lastname = lastname;
        this.score = score;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }


    public List<Professor> getProfessors() {
        return professors;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastname= '" + lastname + '\'' +
                ", score=" + score +
                '}';
    }
}
