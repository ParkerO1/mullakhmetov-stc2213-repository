
import Model.Professor;
import Model.Student;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class Main {
    public static void main(String[] args) {
        EntityManagerFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Professor.class)
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        EntityManager em = factory.createEntityManager();


//        em.getTransaction().begin();
//        em.persist(professor);
//        em.getTransaction().commit();
//
//        em.getTransaction().begin();
//        em.persist(student);
//        em.getTransaction().commit();
//        em.getTransaction().begin();
//        Student student2 = em.find(Student.class, 1L);
//        em.getTransaction().commit();

        em.getTransaction().begin();
        Student student2 = em.find(Student.class, 1L);
        em.getTransaction().commit();

        System.out.println(
                "Ученик  " + student2.toString() +   "\nОбучается у "
                + student2.getProfessors().toString()
        );
    }
}
