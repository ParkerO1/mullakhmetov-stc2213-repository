import java.sql.*;
import java.util.Arrays;
import java.util.List;

public class Main  {
    public static void main(String[] args) throws SQLException  {

        CrudHuman crudHuman = new CrudHuman();
        List<Human> humans = crudHuman.getAllHumans(); //теперь в humans весь список людей из бд
       Human human = crudHuman.getHumanById(0);

        Human human1 = new Human(crudHuman.returnMaxId(crudHuman.getAllHumans()) + 1, "Виктор","Ратиев","Константинович",
                "Нефтеюганск","Петрова","14","122","1245346234");
        crudHuman.createHuman(human1);
//        human.setLastName("Mitrofanovich");
//        crudHuman.updateHuman(human); // теперь значения у данного человека в бд должно измениться
       crudHuman.deleteHuman(human); //после выполнения данной команды - данный человек должен удалиться из бд
            }
       }


