import java.sql.SQLException;
import java.util.List;

public interface CrudHumanImp {
    List getAllHumans(); //Возвращает список всех людей из базы данных
    Human getHumanById(int id); //Возвращает конкретного человека, у которого определенный id
    void createHuman(Human human); //Создает человека и записывает его в БД
    void updateHuman(Human human); //Обновляет данные по конкретному человеку
    void deleteHuman(Human human); //Удаляет конкретного человека
}
