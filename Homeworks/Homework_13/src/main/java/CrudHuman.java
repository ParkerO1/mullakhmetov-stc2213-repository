import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CrudHuman  implements CrudHumanImp{
    DBConnect dbConnect;
    {
        try {
            dbConnect = new DBConnect();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    Connection connection = DBConnect.createConnection();
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;

    @Override
    public List getAllHumans() {

        try {
            preparedStatement = connection.prepareStatement("select * from public.\"crudHuman\" ;");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        List<Human> arrayListHuman = new ArrayList<>();
        while (true) {
            try {
                if (!resultSet.next()) break;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            try {
                Human human = new Human(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("lastname"),
                        resultSet.getString("patronymic"),
                        resultSet.getString("city"),
                        resultSet.getString("street"),
                        resultSet.getString("house"),
                        resultSet.getString("flat"),
                        resultSet.getString("numberPassport")

                );
                arrayListHuman.add(human);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        return arrayListHuman;

    }

    @Override
    public Human getHumanById(int id) {
        Human human = new Human();
        try {
            preparedStatement = connection.prepareStatement("select * from public.\"crudHuman\" where id = ?;");
            preparedStatement.setInt(1,id);
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        while (true) {
            try {
                if (!resultSet.next()){
                    System.out.println("Человека с таким id нет в базе");
                    break;}
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            try {
                 human = new Human(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("lastname"),
                        resultSet.getString("patronymic"),
                        resultSet.getString("city"),
                        resultSet.getString("street"),
                        resultSet.getString("house"),
                        resultSet.getString("flat"),
                        resultSet.getString("numberPassport")
                );
                System.out.println(human.toString());
                return  human;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        return null;
    }

    @Override
    public void createHuman(Human human) {
        try {
            preparedStatement = connection.prepareStatement("insert into public.\"crudHuman\" " +
                            "(id,name,lastname,patronymic,city,street,house,flat,numberpassport) values(?,?,?,?,?,?,?,?,?) ;");
            preparedStatement.setInt(1,human.getId());
            preparedStatement.setString(2, human.getName());
            preparedStatement.setString(3, human.getLastName());
            preparedStatement.setString(4,human.getPatronymic());
            preparedStatement.setString(5,human.getCity());
            preparedStatement.setString(6,human.getStreet());
            preparedStatement.setString(7,human.getHouse());
            preparedStatement.setString(8,human.getFlat());
            preparedStatement.setString(9,human.getNumberPassport());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void updateHuman(Human human) {
        try {
            preparedStatement = connection.prepareStatement("update public.\"crudHuman\" set " +
                    "name = ?,lastname = ?,patronymic = ?,city = ?,street = ?,house = ?,flat = ?,numberpassport =? where id = "
            + human.getId());
            preparedStatement.setString(1, human.getName());
            preparedStatement.setString(2, human.getLastName());
            preparedStatement.setString(3,human.getPatronymic());
            preparedStatement.setString(4,human.getCity());
            preparedStatement.setString(5,human.getStreet());
            preparedStatement.setString(6,human.getHouse());
            preparedStatement.setString(7,human.getFlat());
            preparedStatement.setString(8,human.getNumberPassport());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteHuman(Human human) {
        try {
            preparedStatement = connection.prepareStatement("delete  from public.\"crudHuman\"  where id = ?;");
            preparedStatement.setInt(1,human.getId());
            preparedStatement.executeUpdate();
            System.out.println("Данный пользователь удален из базы");
        } catch (Exception e) {
            System.out.println("Человека с данным id нет в базе");
        }
    }

    public int returnMaxId(List<Human> humanList){
        int temp = 0;
        for (Human human: humanList) {
            if(human.getId() > temp){
                temp = human.getId();
            }
        }
        return temp;
    }
}
