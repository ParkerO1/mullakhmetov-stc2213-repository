import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;



public class DBConnect  {
    private static Connection connection;

    public DBConnect() throws SQLException{
        connection = createConnection();
    }
    public static Connection createConnection  (){
        if(connection == null){
            Properties properties = new Properties();
            try{
                FileInputStream fileInputStream = new FileInputStream("Homeworks/Homework_13/src/main/resources/db.properties");
                properties.load(fileInputStream);

                connection = DriverManager.getConnection(
                        properties.getProperty("url"),
                        properties.getProperty("user"),
                        properties.getProperty("password"));
            }
            catch (IOException e){
                e.printStackTrace();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

            return connection;
        }
        return connection;

    }
}

//public class DBConnect  {
//    public static Connection createConnection  () throws SQLException {
//        Properties properties = new Properties();
//        try{
//            FileInputStream fileInputStream = new FileInputStream("Homeworks/Homework_13/src/main/resources/db.properties");
//            properties.load(fileInputStream);
//        }
//        catch (IOException e){
//            e.printStackTrace();
//        }
//        Connection connection = DriverManager.getConnection(
//                properties.getProperty("url"),
//                properties.getProperty("user"),
//                properties.getProperty("password"));
//
//
//        return connection;
//    }
//}
