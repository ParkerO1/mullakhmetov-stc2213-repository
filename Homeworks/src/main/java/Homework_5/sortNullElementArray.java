package Homework_5;

import java.util.Arrays;

public class sortNullElementArray {
    public static void main(String[] args) {
        int [] array = new  int[] {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        System.out.println(Arrays.toString(array));
        sortElement(array);
        System.out.println(Arrays.toString(array));
    }
    public static void sortElement(int [] array){
        for ( int i = 0; i <= array.length/2; i++){
            for (int j = array.length - 1; j >=array.length/2 ; j--  ){
                if (array[i] < array[j]){
                    int  temp = array [j];
                    array[j] = array[i];
                    array[i] = temp;
                }
            }
        }
    }
}
