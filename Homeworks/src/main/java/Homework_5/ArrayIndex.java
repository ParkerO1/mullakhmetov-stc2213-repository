package Homework_5;

import java.io.IOException;
import java.util.Scanner;

public class ArrayIndex {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        int [] array = new int [] {10,11,12,13,14,15,16,17,18,19};
        System.out.print("Enter the number: " );
        int number = scanner.nextInt();
        int result = indexArray(array,number);
        System.out.print("Number index in array: " +result);
    }


    public static int indexArray (int [] array, int number){
        for(int i =0; i < array.length; i++){
            if (array[i]== number){
                return i;
            }
        }
        return -1;
    }
}