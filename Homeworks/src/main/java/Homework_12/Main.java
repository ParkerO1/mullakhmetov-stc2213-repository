package Homework_12;

import java.util.*;

public class Main {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        String phrase = scanner.nextLine();
        ArrayList<String> arrayList = new ArrayList<>(List.of(phrase.split(" ")));

        Map<String, Integer> map = new HashMap<>();

        for (String word : arrayList) {
            if (!map.containsKey(word)) {
                map.put(word, 1);
            } else {
                int temp;
                temp = map.get(word);
                temp++;
                map.put(word, temp);
            }
        }

        System.out.println(Arrays.toString(arrayList.toArray()));

        for (Map.Entry<String, Integer> item : map.entrySet()) {

            System.out.printf("Слово: %s  встречается - %d раз\n", item.getKey(), item.getValue());
        }
    }
}
