package Homework_10;


import java.util.Random;

public class Main {

    public static void main(String[] args) {

        int[] numbers = new int[50];
        Random random = new Random();
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = random.nextInt(800) + 100;
        }

        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println();
        System.out.println("**************************");
        System.out.print("Четные числа: ");
        Sequence.filter(numbers, x -> x % 2 == 0);
        System.out.println();
        System.out.println("**************************");
        Sequence.filter(numbers, x -> {
            if (((x % 100) +((x/10)%10)+ (x / 100)) % 2 == 0)
                System.out.println("Сумма цифр элемента: " + x + " четная");
            return false;
        });
        System.out.println("*************************");
        Sequence.filter(numbers, x -> {
            if (((x % 100) % 2 == 0)& (((x/10)%10) % 2 == 0) & ((x / 100) % 2 == 0)) {
                System.out.println("Все цифры элемента: " + x + " четные");
            }
            return false;
        });
        System.out.println("*************************");
        Sequence.filter(numbers, x -> {
            if ((x % 100) == (x / 100)) {
                System.out.println("Число: " + x + " палиндром");
            }
            return false;
        });
    }
}
