package Homework_9;

public class Main {
    public static void main(String[] args) {

        Circle circle = new Circle(2);
        Square square = new Square(3);
        Ellipse ellipse = new Ellipse(5,2);
        Rectangle rectangle = new Rectangle(2,3);
        circle.setX(0);
        circle.setY(0);
        square.setX(0);
        square.setY(0);


        Figure[] figures = {circle,square,ellipse,rectangle};

        for (Figure figure:figures) {
            System.out.println(figure.getPerimeter());
        }
        System.out.println("********************************************");
        circle.move(5,2);
        System.out.println();
        square.move(10,12);

    }
}
