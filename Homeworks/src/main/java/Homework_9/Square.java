package Homework_9;

public class Square extends Rectangle implements Movable {

    private double side;

    Square(double side) {
        super(side,side);
        this.side = side;
    }

    @Override
    public void move(int x, int y) {
        System.out.printf("New square center coordinates: X - %d , Y - %d",x,y);
    }

    @Override
    double getPerimeter() {
        return 4 * side;
    }
}
