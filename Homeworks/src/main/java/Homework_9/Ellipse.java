package Homework_9;

public class Ellipse extends Figure {

    private double semiMajorAxis;
    private double semiMinorAxis;

    Ellipse(double semiMajorAxis, double semiMinorAxis) {
        this.semiMajorAxis = semiMajorAxis;
        this.semiMinorAxis = semiMinorAxis;
    }

    @Override
    double getPerimeter() {
        return 4 * ((Math.PI*semiMajorAxis*semiMinorAxis + Math.pow (semiMajorAxis - semiMinorAxis,2))
                /(semiMajorAxis+semiMinorAxis));
    }
}
