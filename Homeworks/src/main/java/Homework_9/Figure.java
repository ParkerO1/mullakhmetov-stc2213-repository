package Homework_9;

public abstract class Figure {
    //    9. ����������� ������ � ����������
//    ������� ����������� ����� Figure, � ������� ������ ���� ��� ���� - x � y ����������.
//    ������ Ellipse � Rectangle ������ ���� ��������� ������ Figure.
//    ����� Square - ������� ������ Rectangle, Circle - ������� ������ Ellipse.
//    � ������ Figure ������������� ����������� ����� getPerimeter().
//    ��� ��, ����� ���������� ��������� Moveable c ������������ ������� .move(int x, int y),
//    ������� �������� ���������� ������ �� �������� ����������.
//    ������ ��������� ������ ����������� ������ ������ Circle � Square.
//    � Main ������� ������ ���� ����� � "������������" �����. � ���� ������� � ������� ��������,
//    � � "������������" ����� �������� ��������� ������� ����������.

    private int x,y;



    abstract double getPerimeter();

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }




}
