package Homework_9;

public class Rectangle extends Figure {

    private double oneSide;
    private double twoSide;

    public Rectangle(double oneSide, double twoSide) {
        this.oneSide = oneSide;
        this.twoSide = twoSide;
    }

    @Override
    double getPerimeter() {
        return 2 * (oneSide +twoSide);
    }
}
