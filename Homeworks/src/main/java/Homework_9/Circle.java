package Homework_9;

public class Circle extends Ellipse implements Movable {

    private double radius;

    Circle(double radius) {
        super(radius,radius);
        this.radius = radius;
    }


    @Override
    double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public void move(int x, int y) {
        System.out.printf("New circle center coordinates: X - %d , Y - %d",x,y);
    }
}
