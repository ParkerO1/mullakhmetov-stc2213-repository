package Homework_8;

public class Lawyer extends Worker {
    private final String whatIsHeDoing = "Отстаивает интересы";

    public Lawyer(String name, String lastName, String profession, int days) {
        super(name, lastName, profession);
        super.setDays(days);
    }

    public void goToWork() {
        super.goToWork();
        System.out.println(" | Род деятельности: " + whatIsHeDoing);
    }


    public void goToVacation(int days) {
        super.goToVacation(days);
    }
}
