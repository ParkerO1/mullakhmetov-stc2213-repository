package Homework_8;

public class Worker {
    private String name;
    private String lastName;
    private String profession;
    private  int days;

    public void setDays(int days) {
        this.days = days;
    }

    public int getDays() {
        return days;
    }

    public void goToWork(){
        System.out.print("Сотрудник: " + name + " " + lastName +
                " |   работает по профессии: " + profession);
    }
    public void goToVacation(int days){
        System.out.println("Сотрудник: " + name + " " + lastName
                + " | работает по профессии: " + profession + " | уходит в отпуск на " + days + " дней");
    }

    public Worker(String name, String lastName, String profession) {
        this.name = name;
        this.lastName = lastName;
        this.profession = profession;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }
}
