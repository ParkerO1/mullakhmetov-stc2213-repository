package Homework_8;


public class Main {
    public static void main(String[] args) {
        Worker[] workers = new Worker[4];
        workers[0] = new Director("Иван", "Иванов", "Директор",60);
        workers[1] = new Programmer("Руслан", "Муллахметов", "Программист", 30);
        workers[2] = new Accountant("Петр", "Петров", "Бухгалтер",35);
        workers[3] = new Lawyer("Константин", "Константинович", "Юрист",25);


        for (Worker value : workers) {
            value.goToWork();
        }

        System.out.println("**********************************************************************************************************");

        for (Worker worker : workers) {
            worker.goToVacation(worker.getDays());
        }
    }
}
