package Homework_8;

public class Accountant extends Worker {
    private final String whatIsHeDoing = "Считает Зарплату";

    public Accountant(String name, String lastName, String profession, int days) {
        super(name, lastName, profession);
        super.setDays(days);
    }

    public void goToWork() {
        super.goToWork();
        System.out.println(" | Род деятельности: " + whatIsHeDoing);
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
    }
}
