package Homework_11;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        FileReader fileReader = new FileReader("Homeworks/src/main/java/Homework_11/Data.txt");
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        Human[] humans = new Human[5];
        while (bufferedReader.ready()) {
            for (int i = 0; i < humans.length; i++) {
                humans[i] = new Human();
                humans[i].setLastName(bufferedReader.readLine());
                humans[i].setName(bufferedReader.readLine());
                humans[i].setPatronymic(bufferedReader.readLine());
                humans[i].setNumberPassport(bufferedReader.readLine());
                humans[i].setCity(bufferedReader.readLine());
                humans[i].setStreet(bufferedReader.readLine());
                humans[i].setHouse(bufferedReader.readLine());
                humans[i].setFlat(bufferedReader.readLine());
            }
            for (int i = 0; i < humans.length; i++) {
                System.out.println(humans[i]);
                System.out.println("********************************");
            }

            for (int i = 0; i < humans.length; i++) {
                for (int j = 1; j < humans.length; j++) {
                    if (humans[i].equals(humans[j])
                            && (humans[i].getLastName() != humans [j].getLastName())
                            && (humans[i].getPatronymic() != humans [j].getPatronymic())
                            && (humans[i].getName() != humans [j].getName())
                    ){
                        System.out.println("У клиента: " + humans[i].getLastName() + " " +
                                humans[i].getName() + " " + humans[i].getPatronymic()+ " совпадает номер паспорта" +
                                " с клиентом " + humans[j].getLastName() + " " +
                                humans[j].getName() + " " + humans[j].getPatronymic());
                    }
                }
            }

        }
        bufferedReader.close();
        fileReader.close();
    }}