package Homework_11;

import java.util.Objects;

public class Human {

    private String name;
    private String lastName;
    private String patronymic;
    private String city;
    private String street;
    private String house;
    private String flat;
    private String numberPassport;

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public void setNumberPassport(String numberPassport) {
        this.numberPassport = numberPassport;
    }

    public String getLastName() {
        return lastName;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    @Override
    public String toString() {
        return lastName + " " + name + " " + patronymic + "\n" +
                "Паспорт: " + "\n" +
                "Серия: " + numberPassport.substring(0,5) + " Номер: " + numberPassport.substring(6) + "\n" +
                "Город: " + city + ", ул. " + street + ", дом " + house + ", квартира " + flat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return Objects.equals(numberPassport, human.numberPassport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberPassport);
    }
}
