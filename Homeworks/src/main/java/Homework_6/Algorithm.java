package Homework_6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.OptionalInt;
import java.util.Random;

public class Algorithm {
    public static void main(String[] args) {
        Random random = new Random ();
        int[] array = new int[random.nextInt(10000)];
        fillArray(array);
        System.out.println(Arrays.toString(array));
        System.out.println();
        minCountNumber(array);
    }


    public static   void fillArray (int[] array){
        for (int i = 0; i < array.length; i++){
                Random random = new Random();
                array[i] = random.nextInt(200) - 100;
        }

    }

    public  static  void minCountNumber(int[] array){
        int[] countArray = new int[200];
        int countNumbers = 10000;
        int numberOfRepetitions = 0;

        for(int i = 0; i < array.length; i++){
        countArray[array[i] + 100]++;
            }
        System.out.println(Arrays.toString(countArray));
        System.out.println();

        for(int i = 0; i < countArray.length; i++){
            if(countArray[i] < countNumbers && countArray[i] !=0){
                countNumbers = countArray[i];
                numberOfRepetitions = i;
            }
        }

        System.out.println("Number: " + (numberOfRepetitions - 100) + " has less recurrences : " + countNumbers);
    }
}
