package Homework_7.Homework_7_with_Comparable;

public class Human implements Comparable {
    private String name;
    private String lastName;
    private int age;

    public Human(String name, String lastName, int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    public Human(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int compareTo(Object object) {
        Human temp = (Human) object;
        return (this.getAge() - temp.getAge());
//        if(this.getAge() < temp.getAge()){
//            // Если текущее меньше входного числа
//            return -1;
//        }
//        if(this.getAge() > temp.getAge()){
//            // Если текущее больше входного числа
//            return 1;
//        }
//        // Если текущее число равно входному числу
//        return 0;
    }
}

