package Homework_7.Homework_7_with_Comparable;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        Human [] arrayHuman = new  Human[random.nextInt(100)];
        for (int i = 0; i < arrayHuman.length; i++){
            arrayHuman[i] = new Human("People " + i, "Ivanovich" + i, random.nextInt(60) + 20);
        }

        Arrays.sort(arrayHuman);
        for (int i = 0; i < arrayHuman.length; i++) {
            System.out.println("firstName: " + arrayHuman[i].getName() +
                    " secondName: " + arrayHuman[i].getLastName()
                    + " NumberOfYears: " +  arrayHuman[i].getAge());
        }
    }

}
