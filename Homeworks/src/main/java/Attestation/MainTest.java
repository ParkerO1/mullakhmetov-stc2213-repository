package Attestation;

public class MainTest {
    public static void main(String[] args) throws Exception {
        UsersRepositoryFileImpl usersRepFile = new UsersRepositoryFileImpl();
        User user1 = new User("Ruslan", "Mullahmetov", 31, true);
        usersRepFile.create(user1);
        User user2 = new User("Nastya", "Karlova", 23, true);
        usersRepFile.create(user2);
        User user3 = new User("Ivan", "Ivanov", 87, true);
        usersRepFile.create(user3);
       usersRepFile.delete(10);
        User user4 = new User("AAA", "AAA", 13, true);
        usersRepFile.create(user4);
        User testupdate = usersRepFile.findById(4);
        testupdate.setAge(125);
        testupdate.setLastname("AHAHAHA");
        usersRepFile.update(testupdate);
    }
}
