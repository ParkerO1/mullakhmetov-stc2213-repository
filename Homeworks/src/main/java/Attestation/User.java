package Attestation;

public class User {
    private String name,lastname;
    private int age;
    private boolean works;

    @Override
    public String toString() {
            return   name + "|" + lastname + "|" + age + "|" + works + "\n";
    }

    public User(String name, String lastname, int age, boolean works) {
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.works = works;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWorks() {
        return works;
    }

    public void setWorks(boolean works) {
        this.works = works;
    }
}

