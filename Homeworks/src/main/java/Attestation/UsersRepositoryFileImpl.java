package Attestation;


import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class UsersRepositoryFileImpl implements UsersRepositoryFile {
    HashMap<Integer, User> map = new HashMap<>();
    Set<Map.Entry<Integer, User>> entrySet = map.entrySet();
    static File file = new File("Homeworks/src/main/java/Attestation/Base.txt");


    @Override
    public User findById(int id) {
        getUserMapFromeFile();

        for (Map.Entry<Integer, User> pair : entrySet) {
            if (pair.getKey() == id)
                return pair.getValue();
        }
        return null;
    }

    @Override
    public void create(User user) {
        GenerateID generateID = new GenerateID();
        map.put(generateID.returnID(), new User(user.getName(), user.getLastname(), user.getAge(), user.isWorks()));
        writeFromMapToBase(map);
    }

    @Override
    public void update(User user) {
        writeFromMapToBase(map);
    }

    @Override
    public void delete(int id) {
        if(map.containsKey(id))
            map.remove(id);
        else
            System.out.printf("Пользователя с ID - %d не существует",id);
        writeFromMapToBase(map);

    }

    // Запись из мапы в базу
    public void writeFromMapToBase(HashMap<Integer, User> map) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            for (Map.Entry<Integer, User> pair : entrySet) {
                {
                    bufferedWriter.write(pair.getKey() + "|" + pair.getValue().toString());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Запись из файла в мапу

    public HashMap<Integer, User> getUserMapFromeFile() {
        try {
            for (String userInBase : Files.readAllLines(Path.of(file.getPath()))) {
                String[] arrayUser = userInBase.split("\\|");
                map.put(Integer.parseInt(arrayUser[0]), new User(arrayUser[1], arrayUser[2],
                        Integer.parseInt(arrayUser[3]), Boolean.parseBoolean(arrayUser[4])));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return map;
    }
}
